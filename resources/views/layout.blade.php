<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" type="text/css" href="@yield('css')">
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
</head>

<body>
    <?php $id = $_SERVER['REQUEST_URI']; ?>
{{--    <div id="grid-container">--}}
        <header>
            <div class="nav-container">
                <a href="{{ route('index') }}" data-title="Витрина" id="nav-container-a-logo">
                    <img class="header-logo" alt="logo" src="{{ asset('images/logo.png') }}"
                    <?php if ($id == "/"){echo'id="selectedLogo"';} ?>/>
                </a>
                <label for="toggle-1" class="toggle-menu">
                    <ul id="label-ul">
                        <li class="label-ul-li" id="mobile-line-red"></li>
                        <li class="label-ul-li" id="mobile-line-darkblue"></li>
                        <li class="label-ul-li" id="mobile-line-blue"></li>
                    </ul>
                </label>
                <input type="checkbox" id="toggle-1">

                <nav>
                    <ul id="menu-ul">
                        <li class="menu-ul-li">
                            <a href="{{ route('about') }}" class="menu-ul-li-a" data-title="О нас">
                                <img class="menu-ul-li-a-img" alt="about" src="{{ asset('images/about.png') }}"
                                <?php if ($id === "/about"){echo'id="selectedMenuItem"';} ?>/>
                            </a>
                        </li>

                        <li class="menu-ul-li">
                            <a href="{{ route('cart') }}" class="menu-ul-li-a" data-title="Карзина">
                                <img class="menu-ul-li-a-img" alt="cart" src="{{ asset('images/cart.png') }}"
                                <?php if ($id === "/cart"){echo'id="selectedMenuItem"';} ?>/>
                            </a>
                        </li>

                        <li class="menu-ul-li">
                            <a href="{{ route('contact') }}" class="menu-ul-li-a" data-title="Контакты">
                                <img class="menu-ul-li-a-img" alt="contact" src="{{ asset('images/contact.png') }}"
                                <?php if ($id === "/contact"){echo'id="selectedMenuItem"';} ?>/>
                            </a>
                        </li>

                        <li class="menu-ul-li">
                            <a href="{{ route('delivery') }}" class="menu-ul-li-a" data-title="Доставка">
                                <img class="menu-ul-li-a-img" alt="delivery" src="{{ asset('images/delivery.png') }}"
                                <?php if ($id === "/delivery"){echo'id="selectedMenuItem"';} ?>"/>
                            </a>
                        </li>

                        <li class="menu-ul-li">
                            <a href="{{ route('payment') }}" class="menu-ul-li-a" data-title="Оплата">
                                <img class="menu-ul-li-a-img" alt="payment" src="{{ asset('images/payment.png') }}"
                                <?php if ($id === "/payment"){echo'id="selectedMenuItem"';} ?>/>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>

        <main id='content'>
            @yield('content')
        </main>

        <footer>
            <div id="footer-flex-container">
                <div>
                    <img id="footer-logo" alt="logo-footer" src="{{ asset('images/logo.png') }}"/>
                </div>

                <ul id="footer-phones-ul">
                    <li>(069) 666 66 66</li>
                    <li id="footer-phones-ul-li-second-child">(069) 696 69 69</li>
                    <li id="footer-phones-ul-li-last-child">(069) 666 66 69</li>
                </ul>

                <div class="footer-flex-container-item">
                    <p id="footer-email">best.shop@gmail.com</p>
                </div>

                <div class="footer-flex-container-item">
                    <p id="footer-copyright">&copy: 2020. Все права проигнорировоны</p>
                </div>
            </div>
        </footer>
{{--    </div>--}}
</body>
</html>
