@extends('layout')

@section('title') {{ $paymentInfo->title }} @endsection

@section('css')

@endsection

@section('content')
    <h1>{{ $paymentInfo->name }}</h1>
    <hr id="line"/>
    {!! $paymentInfo->content !!}
@endsection
