@extends('layout')

@section('title') {{ $contactInfo->title }} @endsection

@section('css') {{ asset('css/pages/contact.css') }} @endsection

@section('content')
    <h1>{{ $contactInfo->name }}</h1>
    <hr id="line"/>
    {!! $contactInfo->content !!}
@endsection
