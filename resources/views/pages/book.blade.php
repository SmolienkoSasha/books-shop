@extends('layout')

@section('title') {{ $book->name }} @endsection

@section('css') {{ asset('css/pages/book.css') }} @endsection

@section('content')
    <h1>{{ $book->name }}</h1>
    <hr id="line"/>
    <div id="book-general-info">
        <img id="book-img" alt="{{ $book->img_alt }}" src="{{ $book->img_src }}"/>
{{--        <h2>{{ $book->name }}</h2>--}}
        <div id="book-general-info-items">
            <h2>Характеристики книги</h2>
            <p>Автор: <strong>{{ $book->author }}</strong></p>
            <p>Год издания: <strong>{{ $book->year }}</strong></p>
            <p>Количество страниц: <strong>{{ $book->pages }}</strong></p>
            <p>Язык: <strong>{{ $book->language }}</strong></p>
            <p>Переплёт: <strong>{{ $book->binding }}</strong></p>
            <p>Формат: <strong>{{ $book->shape }}</strong></p>
            <p>Вес: <strong>{{ $book->weight }}</strong></p>
            <p>Цена: <strong>{{ $book->price }} {{ $book->currency }}</strong></p>
            <form action="{{ route('cart-add', $book) }}" method="POST">
                <button id="button-cart" type="submit" role="button">в корзину</button>
                @csrf
            </form>
        </div>
    </div>

    <div id="book-description">
        {!! $book->description !!}
    </div>
@endsection
