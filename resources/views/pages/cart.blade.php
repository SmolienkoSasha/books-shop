@extends('layout')

@section('title', ' Корзина')

@section('css') {{ asset('css/pages/cart.css') }} @endsection

@section('content')
    <h1>Корзина</h1>
    <hr id="line"/>
    <div>
        <table>
            <thead>
                <tr>
                    <th>Название</th>
                    <th>Кол-во</th>
                    <th>Цена</th>
                    <th>Стоимость</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @foreach($customer->books as $book)
                <tr class="order-line-tr">
                    <td>
                        <img class="book-img" alt="{{ $book->img_alt }}" src="{{ $book->img_src }}">
                        <span class="book-name">{{ $book->name }}</span>
                    </td>
                    <td class="count-td">
                        <div>
                            <form class="form-btn-count" action="{{ route('cart-add', $book) }}" method="POST">
                                <button type="submit" class="btn-count" id="btn-plus">
                                    <span>+</span>
                                </button>
                                @csrf
                            </form>
                            <div class="form-btn-count">
                                <span class="count-span">{{ $book->pivot->count }}</span>
                            </div>
                            <form class="form-btn-count" action="{{ route('cart-remove', $book) }}" method="POST">
                                <button type="submit" class="btn-count"  id="btn-minus">
                                    <span>-</span>
                                </button>
                                @csrf
                            </form>
                        </div>
                    </td>
                    <td class="price-td">{{ $book->price }} </td>
                    <td class="full-item-price-td">{{ $book->getPriceForCount() }} {{ $book->currency }}</td>
                    <td class="del-td">
                        <form class="form-btn-count delete-button" action="{{ route('cart-delete', $book) }}" method="POST">
                            <button type="submit" class="btn-count"  id="btn-delete">
                                <span>x</span>
                            </button>
                            @csrf
                        </form>
                    </td>
                </tr>
            @endforeach
                <tr>
                    <td colspan="3" id="full-price-text-td">Общая стоимость:</td>
                    <td id="full-price-value-td">{{ $customer->calculateFullSum() }} {{ $book->currency }}</td>
                </tr>
            </tbody>
        </table>
        <br>
        <div>
            <a type="button" id="button" href="{{ route('order-get-data') }}">Оформить заказ</a>
        </div>
    </div>
@endsection
