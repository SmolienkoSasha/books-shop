@extends('layout')

@section('title', ' Подтверждение e-mail')

@section('css') {{ asset('css/pages/emailConfirm.css') }} @endsection

@section('content')
    @dump( session('emailVerification'))
    <h1>Подтвердите заказ</h1>
    <hr id="line"/>
    <div class="container1">
        @if(session()->has('warning'))
            <p class="alert-warning">{{ session()->get('warning') }}</p>
        @endif
        @error('code')
        <p class="alert-warning inputs-checking">{{ $message }}</p>
        @enderror
        <form action="{{ route('order-save') }}" method="POST">
            <label>
                <p>Введите код, который был выслан вам на указанную почту:</p>
                <input type="text" name="code" id="code"  placeholder="666a69" value="{{ old('code') }}">
            </label>
            <button type="submit">Подтвердить</button>
            @csrf
        </form>

    </div>
@endsection
