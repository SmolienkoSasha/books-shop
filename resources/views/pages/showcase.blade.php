@extends('layout')

@section('title') Витрина @endsection

@section('css') {{ asset('css/pages/showcase.css') }} @endsection

@section('content')
    @if(session()->has('success'))
        <p id="alert-success">{{ session()->get('success') }}</p>
    @endif

    @if(session()->has('warning'))
        <p id="alert-warning">{{ session()->get('warning') }}</p>
    @endif

    <h1>Наша продукция</h1>
    <hr id="line"/>

    <form method="GET" action="{{ route('index') }}">
        <div id="filters-flex-container">
            <div class="filters-flex-container-items" id="filters-flex-container-item-prices">
                <label for="price_from">Цена от
                    <input type="text" name="price_from" id="price_from" size="6" value="{{ request()->price_from }}">
                </label>
                <label for="price_to">до
                    <input type="text" name="price_to" id="price_to" size="6"  value="{{ request()->price_to }}">
                </label>
            </div>

            <div class="filters-flex-container-items">
                <input type="checkbox" name="new" id="new" @if(request()->has('new')) checked @endif>
                <label for="new">Новинка</label>
            </div>
            <div class="filters-flex-container-items">
                <input type="checkbox" name="hit" id="hit" @if(request()->has('hit')) checked @endif>
                <label for="hit">Хит</label>
            </div>
            <div class="filters-flex-container-items">
                <input type="checkbox" name="recommend" id="recommend" @if(request()->has('recommend')) checked @endif>
                <label for="recommend">Рекомендуем</label>
            </div>

             <div class="filters-flex-container-items">
                <label>Язык
                    <select name="language" id="filters-flex-container-item-language">
                        <option></option>
                        <option @if(request()->language === "Русский") selected @endif>Русский</option>
                        <option @if(request()->language === "Украинский") selected @endif>Украинский</option>
                        <option @if(request()->language === "Английский") selected @endif>Английский</option>
                    </select>
                </label>
            </div>

            <div class="filters-flex-container-items">
                <button type="submit" class="filter-buttons" id="filter-button-find">Поиск</button>
                <a href="{{ route('index') }}" class="filter-buttons" id="filter-button-drop">Сброс</a>
            </div>
        </div>
    </form>

    <div id="books-flex-container">
        @foreach($books as $book)
            @if ($book->available)
                <a class="books-flex-container-item" href="{{ route('book', $book->id) }}">
                    <div id="labels">
                        @if($book->new)
                            <span class="label label-new">Новинка</span>
                        @endif

                        @if($book->hit)
                            <span class="label label-hit">Хит продаж!</span>
                        @endif

                        @if($book->recommend)
                            <span class="label labels-recommend">Рекомендуем</span>
                        @endif
                    </div>
                    <img class="book-img" alt="{{ $book->img_alt }}" src="{{ $book->img_src }}" />
                    <br/>
                    <p>{{ $book->name }}</p>
                    <p class="book-author">{{ $book->author }}</p>
                    <p class="book-price"><strong>{{ $book->price }} {{ $book->currency }}</strong></p>
                    <form action="{{ route('cart-add', $book) }}" method="POST">
                        <button class="button-cart" type="submit" role="button">в корзину</button>
                        @csrf
                    </form>
                </a>
            @endif
        @endforeach
    </div>
    {{ $books->links() }}
@endsection
