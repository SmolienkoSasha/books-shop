@extends('layout')

@section('title', 'Оформить заказ')

@section('css') {{ asset('css/pages/order.css') }} @endsection

@section('content')
    <h1>Оформление заказа</h1>
    <hr id="line"/>

    <div id="order-container">
        <form  action="{{ route('order-confirm') }}" method="POST">
            <p><strong>Укажите свои данные, чтобы наш менеджер мог с вами связаться:</strong></p>
            <div>
                <label>
                    <p class="headLine">Имя:</p>
                    <input class="inputs" type="text" placeholder="Lucifer" name="name"
                           id="name" value="{{ old('name') }}">
                    @error('name')
                    <p class="alert-warning inputs-checking">{{ $message }}</p>
                    @enderror
                </label>
            </div>

            <div>
                <label>
                    <p class="headLine">Номер телефона:</p>
                    <input class="inputs" placeholder="066-666-69-99" type="tel" name="phone"
                           id="phone" value="{{ old('phone') }}">
                    @error('phone')
                    <p class="alert-warning inputs-checking">{{ $message }}</p>
                    @enderror
                </label>
            </div>

            <div>
                <label>
                    <p class="headLine">E-mail:</p>
                    <input class="inputs" placeholder="Satana@gmail.com" type="text"
                           name="email" id="email" value="{{ old('email') }}">
                    @error('email')
                    <p class="alert-warning inputs-checking">{{ $message }}</p>
                    @enderror
                </label>
            </div>

            <div>
                <label>
                    <p class="headLine">Доставка:</p>
                    <select class="inputs" id="delivery" name="delivery">
                        <option selected>Самовывоз</option>
                        <option>Курьерская доставка по Херсону</option>
                        <option>Новая почта</option>
                        <option>Укрпочта</option>
                    </select>
                </label>
            </div>

            <div>
                <label>
                    <p class="headLine">Оплата:</p>
                    <select class="inputs" id="payment" name="payment">
                        <option>Наличные</option>
                        <option>Kарта</option>
                    </select>
                </label>
            </div>

            <script src="{{ asset('js/drawPayment.js') }}"></script>
            <script>
                let selectorPayment = document.getElementById('payment');
                let paymentOptions = selectorPayment.options;
                let selectorDelivery = document.getElementById('delivery');

                drawPayment(selectorPayment, paymentOptions, selectorDelivery);
            </script>

            <br/>
            <br>
            @csrf
            <button id="confirm-order-btn">Подтвердить заказ</button>
        </form>
    </div>

@endsection
