@extends('layout')

@section('title') {{ $aboutInfo->title }} @endsection

@section('css')

@endsection

@section('content')
    <h1>{{ $aboutInfo->name }}</h1>
    <hr id="line"/>
    {!! $aboutInfo->content !!}
@endsection
