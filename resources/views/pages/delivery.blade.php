@extends('layout')

@section('title') {{ $deliveryInfo->title }} @endsection

@section('css')

@endsection

@section('content')
    <h1>{{ $deliveryInfo->name }}</h1>
    <hr id="line"/>
    {!! $deliveryInfo->content !!}
@endsection
