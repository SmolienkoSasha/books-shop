function drawPayment(selectorPayment, paymentOptions, selectorDelivery) {
    selectorDelivery.onchange = function () {
        while (paymentOptions.length > 0) {
            selectorPayment.removeChild(paymentOptions[paymentOptions.length-1]);
        }

        let deliverySelectedOption = selectorDelivery.options[selectorDelivery.selectedIndex].text;
        let arrayPaymentOptions;

        if (deliverySelectedOption === 'Самовывоз') {
            arrayPaymentOptions = ['Наличные', 'Kарта'];
        } else if (deliverySelectedOption === 'Новая почта' || deliverySelectedOption === 'Укрпочта') {
            arrayPaymentOptions = ['Наложенный платеж', 'Kарта', 'Счет-фактура'];
        } else {
            arrayPaymentOptions = ['Наличные'];
        }

        for (let i = 0; i < arrayPaymentOptions.length; i++) {
            let option = document.createElement("option");
            option.text = arrayPaymentOptions[i];
            selectorPayment.appendChild(option);
        }
    };
}
