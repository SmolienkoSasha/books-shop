<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 155);
            $table->string('author', 155);
            $table->string('language', 100);
            $table->string('binding', 50);
            $table->string('shape', 50);
            $table->string('weight', 50);
            $table->string('img_src');
            $table->string('img_alt', 50);
            $table->unsignedInteger('year')->nullable();
            $table->unsignedInteger('pages')->nullable();
            $table->double('price', 8, 2);
            $table->string('currency', 10);
            $table->boolean('available')->nullable();
            $table->text('description')->nullable();
            $table->boolean('new')->default(false);
            $table->boolean('hit')->default(false);
            $table->boolean('recommend')->default(false);
            $table->unsignedInteger('availableAmount')->default(0);;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
