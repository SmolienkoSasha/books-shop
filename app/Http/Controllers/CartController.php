<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Classes\Cart;
use App\Models\Customer;


class CartController extends Controller
{
    public function index()
    {
        $cart = new Cart();
        $customer = $cart->getCustomer();

        return view('pages.cart', ['customer' => $customer]);
    }

    public function addOrIncreaseAmount(Book $book)
    {
//        Customer::eraseOrderSessions();
        $cart = new Cart();
        $cart->addOrIncreaseAmount($book);

        return redirect()->route('cart');
    }

    public function removeOrReduceAmount(Book $book)
    {
        $cart = new Cart();
        $cart->removeOrReduceAmount($book);

        return redirect()->route('cart');
    }

    public function delete(Book $book)
    {
        $cart = new Cart();
        $cart->delete($book);

        return redirect()->route('cart');
    }
}
