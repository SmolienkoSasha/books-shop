<?php

namespace App\Http\Controllers;

use App\Http\Requests\BooksFilterRequest;
use App\Models\Book;

class ShowcaseController extends Controller
{
    public function index(BooksFilterRequest $request)
    {
        $booksQuery = Book::query();

        if ($request->filled('price_from')) {
            $booksQuery->where('price', '>=', $request->price_from);
        }

        if ($request->filled('price_to')) {
            $booksQuery->where('price', '<=', $request->price_to);
        }


        if ($request->filled('language')) {
            $booksQuery->where('language', $request->language);
        }


        foreach (['hit', 'new', 'recommend'] as $field) {
            if ($request->has($field)) {
                $booksQuery->where($field, true);
            }
        }

        $books = $booksQuery->paginate(15)->withPath("?" . $request->getQueryString());

        return view('pages.showcase', ['books' => $books]);
    }

    public function show(string $id)
    {
        $book = Book::where('id', $id)->first();
        return view('pages.book', ['book' => $book]);
    }
}
