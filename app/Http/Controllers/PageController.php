<?php

namespace App\Http\Controllers;

use App\Models\Page;

class PageController extends Controller
{
    public function about()
    {
        $aboutInfo = Page::where('slug', 'about')->first();
        return view('pages.about', ['aboutInfo' => $aboutInfo]);
    }

    public function contact()
    {
        $contactInfo = Page::where('slug', 'contacts')->first();
        return view('pages.contact', ['contactInfo' => $contactInfo]);
    }

    public function delivery()
    {
        $deliveryInfo = Page::where('slug', 'delivery')->first();
        return view('pages.delivery', ['deliveryInfo' => $deliveryInfo]);
    }

    public function payment()
    {
        $paymentInfo = Page::where('slug', 'payment')->first();
        return view('pages.payment', ['paymentInfo' => $paymentInfo]);
    }
}
