<?php

namespace App\Http\Controllers;

use App\Classes\Cart;
use App\Classes\Order;
use App\Http\Requests\OrderGetDataRequest;
use App\Http\Requests\OrderConfirmEmailRequest;

class OrderController extends Controller
{
    public function getData()
    {
        return view('pages.orderGetData');
    }

    public function confirm(OrderGetDataRequest $request)
    {
        $cart = new Cart();
        $customer = $cart->getCustomer();

        $order = new Order();
        $order->emailVerification($request->email);
        $order->getData($request, $customer->id);

        return redirect()->route('order-confirm-redirect');
    }

    public function confirmRedirect()
    {
        return view('pages.confirm');
    }

    public function save(OrderConfirmEmailRequest $request)
    {
        $emailVerification = session('emailVerification');

        if ($request->code === $emailVerification) {
            $orderData = session('orderData');
            $cart = new Cart();
            $customer = $cart->getCustomer();

            $order = new Order();
            $order->saveData($customer, $orderData);
            session()->flash('success', 'Ваш заказ приният в обработку, наш менеджер свяжется с вами');

            return redirect()->route('index');
        } else {
            session()->flash('warning', 'Неверный код, попробуйте еще раз');

            return redirect()->route('order-confirm-redirect');
        }
    }
}
