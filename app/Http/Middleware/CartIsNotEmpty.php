<?php

namespace App\Http\Middleware;

use App\Models\Customer;
use Closure;

class CartIsNotEmpty
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $customerId = session('customerId');

        if (!is_null($customerId)) {
            $customer = Customer::findOrFail($customerId);
            if ($customer->books->count() > 0) {
                return $next($request);
            }
        }

        session()->flash('warning', 'Ваша корзина пуста!');
        return redirect()->route('index');
    }
}
