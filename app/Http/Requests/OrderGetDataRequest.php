<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderGetDataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'alpha', 'min:3', 'max:150'],
            'phone' => ['required', 'size:13', 'regex:/^(0)[0-9]{2}-[0-9]{3}-[0-9]{2}-[0-9]{2}$/'],
            'email' => ['required', 'min:4', 'max:150', 'regex:/[^@]+@[^@]+\.[a-zA-Z]{2,6}/'],
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Поле :attribute обязательно для ввода',
            'min' => 'Поле :attribute должно иметь минимум :min символов',
            'max' => 'Поле :attribute должно иметь максимум :max символов',
            'regex' => 'Поле :attribute должно соответствовать определленому формату, показанному в примере',
            'alpha' => 'Поле :attribute должно содержать только буквенные символы',
            'size' => 'Поле :attribute должно :size символов'
        ];
    }
}
