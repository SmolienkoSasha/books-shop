<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    public function getPriceForCount():float
    {
        if (!is_null($this->pivot)) {
            return $this->pivot->count * $this->price;
        }

        return $this->price;
    }
}
