<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function books():object // special laravel object that allows us to work with pivot table
    {
        return $this->belongsToMany(Book::class)
                    ->withPivot('count', 'payment', 'delivery')
                    ->withTimestamps();
    }

    public function calculateFullSum():float
    {
        $sum = 0;

        foreach ($this->books as $book) {
            $sum += $book->getPriceForCount();
        }

        return $sum;
    }

    public function isCustomerAlreadyExist($customersFromDB, array $orderData)
    {
        foreach ($customersFromDB as $customerFromDB) {
            if ($customerFromDB->name === $orderData['customerName'] &&
                ($customerFromDB->phone === $orderData['customerPhone'] ||
                    $customerFromDB->email === $orderData['customerEmail'])
            ) {
                return $customerFromDB;
            }

            $result = false;
        }
        return $result;
    }

    public function saveCustomer(string $name, string $phone, string $email):void
    {
        if ($this->name === null) {
            $this->name = $name;
            $this->phone = $phone;
            $this->email = $email;
            $this->save();
        }
    }

    public function savePivot(string $payment, string $delivery):void
    {
        foreach ($this->books as $book) {
            $pivotRow = $this->books()->where('book_id', $book->id)->first()->pivot;
            if ($pivotRow->payment === null && $pivotRow->delivery === null) {
                $pivotRow->payment = $payment;
                $pivotRow->delivery = $delivery;
                $pivotRow->update();
            }
        }
    }

    public function changePivotTable(Customer $customer, array $orderData, Customer $customerAlreadyExist):void
    {
        $customerBooks = [];

        foreach ($customer->books as $itemBook) {
            $pivotRow = $customer->books()->where('book_id', $itemBook->id)->first()->pivot;
            $customerBooks[] = ['bookId' => $itemBook->id, 'bookCount' => $pivotRow->count];
            $customer->books()->detach($itemBook->id);
        }
        Customer::destroy($customer->id);

        for ($x = 0; $x < count($customerBooks); $x++) {
            $customerAlreadyExist->books()->attach($customerBooks[$x]['bookId'],
                [
                    'book_id' => $customerBooks[$x]['bookId'],
                    'count' => $customerBooks[$x]['bookCount'],
                    'payment' => $orderData['pivotPayment'],
                    'delivery' => $orderData['pivotDelivery']
                ]
            );
        }
    }

    public static function getFullSum():float
    {
        return session('full_order_sum', 0);
    }

    public static function changeFullSum(float $changeSum):void
    {
        $sum = self::getFullSum() + $changeSum;
        session(['full_order_sum' => $sum]);
    }

    public static function eraseOrderSessions():void
    {
        session()->forget('customerId');
        session()->forget('emailVerification');
        session()->forget('orderData');
        session()->forget('full_order_sum');
    }
}
