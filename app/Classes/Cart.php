<?php


namespace App\Classes;

use App\Models\Customer;
use App\Models\Book;

class Cart
{
    private $customer;

    public function __construct()
    {
        $customerId = session('customerId');
        if (is_null($customerId)) {
            $this->customer = Customer::create();
            session(['customerId' => $this->customer->id]);
        } else {
            $this->customer = Customer::findOrFail($customerId);
        }
    }

    public function getCustomer():Customer
    {
        return $this->customer;
    }

    private function getPivotRow(Book $book):object // special laravel object that allows us to work with pivot raw
    {
        return $this->customer->books()->where('book_id', $book->id)->first()->pivot;
    }

    public function addOrIncreaseAmount(Book $book):void
    {
        if ($this->customer->books->contains($book->id)) {
            $pivotRow = $this->getPivotRow($book);
            $pivotRow->count++;
            $pivotRow->update();
        } else {
            $this->customer->books()->attach($book->id);
        }
        Customer::changeFullSum($book->price);
    }

    public function removeOrReduceAmount(Book $book):void
    {
        //remove repeated parts from removeOrReduceAmount and delete methods
        if ($this->customer->books->contains($book->id)) {
            $pivotRow = $this->getPivotRow($book);
            if ($pivotRow->count === 1) {
                $this->customer->books()->detach($book->id);
            } else {
                $pivotRow->count--;
                $pivotRow->update();
            }
        }
        Customer::changeFullSum(-$book->price);
    }

    public function delete(Book $book):void
    {
        //remove repeated parts from removeOrReduceAmount and delete methods
        if ($this->customer->books->contains($book->id)) {
            $this->customer->books()->detach($book->id);
        }
        Customer::changeFullSum(-$book->price);
    }
}
