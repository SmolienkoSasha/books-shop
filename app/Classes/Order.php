<?php

namespace App\Classes;

use App\Models\Customer;
use App\Http\Requests\OrderGetDataRequest;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;


class Order
{
    public function emailVerification(string $email):void
    {
        $emailVerificationBytes = openssl_random_pseudo_bytes(3);
        $emailVerificationString = bin2hex($emailVerificationBytes);
        $message = "Ваш код подтверждения: $emailVerificationString";

        Mail::raw($message, function(Message $mail) use ($email) {
            $mail->subject('Код подтверждения заказа');
            $mail->from('Kartoha774@gmail.com', 'book-shop');
            $mail->to($email);
        });

        session(['emailVerification' => $emailVerificationString]);
    }

    public function getData(OrderGetDataRequest $request, int $customerId):void
    {
        $orderData = [
            'customerId' => $customerId,
            'customerName' => $request->name,
            'customerPhone' => $request->phone,
            'customerEmail' => $request->email,
            'pivotPayment' => $request->payment,
            'pivotDelivery' => $request->delivery
        ];

        session(['orderData' => $orderData]);
    }


    public function saveData(Customer $customer, array $orderData):void
    {
        $customersFromDB = Customer::all();
         $customerAlreadyExist = $customer->isCustomerAlreadyExist($customersFromDB, $orderData);

        if ($customerAlreadyExist) {
            $customerAlreadyExist->changePivotTable($customer, $orderData, $customerAlreadyExist);
        } else {
            $customer->saveCustomer($orderData['customerName'], $orderData['customerPhone'], $orderData['customerEmail']);
            $customer->savePivot($orderData['pivotPayment'], $orderData['pivotDelivery']);
        }

        Customer::eraseOrderSessions();
    }
}
