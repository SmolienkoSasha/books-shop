<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ShowcaseController@index')->name('index');
Route::get('/book/{id}', 'ShowcaseController@show')->name('book');


Route::post('/cart/add/{book}', 'CartController@addOrIncreaseAmount')->name('cart-add');

Route::group(['middleware' => 'cart_not_empty'], function () {
    Route::get('/cart', 'CartController@index')->name('cart');
    Route::post('/cart/remove/{book}', 'CartController@removeOrReduceAmount')->name('cart-remove');
    Route::post('/cart/delete/{book}', 'CartController@delete')->name('cart-delete');
    Route::get('/order/get', 'OrderController@getData')->name('order-get-data');
    Route::post('/order/confirm', 'OrderController@confirm')->name('order-confirm');
    Route::get('/order/confirm/redirect', 'OrderController@confirmRedirect')->name('order-confirm-redirect');
    Route::post('/order/save', 'OrderController@save')->name('order-save');
 });


Route::get('/about', 'PageController@about')->name('about');
Route::get('/contact', 'PageController@contact')->name('contact');
Route::get('/delivery', 'PageController@delivery')->name('delivery');
Route::get('/payment', 'PageController@payment')->name('payment');
